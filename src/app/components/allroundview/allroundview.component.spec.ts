import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllroundviewComponent } from './allroundview.component';

describe('AllroundviewComponent', () => {
  let component: AllroundviewComponent;
  let fixture: ComponentFixture<AllroundviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllroundviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllroundviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
