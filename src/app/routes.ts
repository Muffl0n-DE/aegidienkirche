import { NotfoundComponent } from './components/notfound/notfound.component';
import { HistoryComponent } from './components/history/history.component';
import { HomeComponent } from './components/home/home.component';
import { Routes } from '@angular/router';
import { EventComponent } from './components/event/event.component';
import { DonateComponent } from './components/donate/donate.component';
import { ContactComponent } from './components/contact/contact.component';
import { GdprComponent } from './components/gdpr/gdpr.component';
import { AllroundviewComponent } from './components/allroundview/allroundview.component';
import { ImpressumComponent } from './components/impressum/impressum.component';

export const appRoutes: Routes = [
    { path: '', redirectTo: 'startseite', pathMatch: 'full'},
    { path: 'startseite', component: HomeComponent},
    { path: 'geschichte', component: HistoryComponent },
    { path: 'neuigkeiten', component: EventComponent },
    { path: 'spenden', component: DonateComponent },
    { path: 'kontakt', component: ContactComponent },
    { path: 'impressum', component: ImpressumComponent },
    { path: 'allroundview', component: AllroundviewComponent },
    { path: 'datenschutz', component: GdprComponent },
    { path: '**', component: HomeComponent }
];
